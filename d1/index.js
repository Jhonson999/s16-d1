// While Loop

let count = 5; //number of iteration

// while(/*condition*/){ //condition - evaluates a given code if it is true or false - if true - the loop will start and continue our iteration or the repetition of block of code, but if the condition is false the loop or repetetion will stop
	//block of code
	//counter for our iteration - reason for continuous loop/iteration

/*example
 Instructions: Repeat a name "Sylvan" 5x
 */

 while(count !== 0) {
 	console.log("Sylvan");
 	count--; //will be decremented by 1
}

/* Instruction: Print numbers 1 to 5 using while */

let number = 1;
        //1 <= 5?
while( number <=5){ //if the number is less than or equal to 5 then procede to iteration
	console.log(number);
	number++;
}

/*Instruction: with a given array, kindly print each element using while loop*/

let fruits = ['Banana','Mango']
// fruits [0]
// fruits [1]

let indexNumber = 0; // We will use this variable as our reference to the index number of our given array

while(indexNumber <= 1){ // the condition is based on the last index of elements that we have on an array
	console.log(fruits[indexNumber]); //kukunin yung elements sa loob ng array base sa indexNumber value
	indexNumber++;
}

let mobilePhones = ['Samsung Galaxy S21','Iphone 13 Pro','Xioami','Realme C','Huawei','Nova 8','Asus Rog 6'];

console.log(mobilePhones.length);

console.log(mobilePhones.length -1); // will give the last index position af an element inside an array

console.log(mobilePhones[mobilePhones.length -1]);

let indexNumberForMobile = 0;

while(indexNumberForMobile <= mobilePhones.length-1){
	console.log(mobilePhones[indexNumberForMobile]);
	indexNumberForMobile++;
}

/* Do-While - do the statement once, before going to the condition*/

let countA = 1;

do{ //execute the statement
	console.log("Juan");
	countA++; //1+1 = 2
}while(countA <= 6); //2 <=6? true
// ============================================================
let countB = 6;
do{
	console.log(`Do-While count ${countB}`);
	countB--;
} while(countB == 7);


// Versus

while(countB == 7){
	console.log(`While count ${countB}`);
	countB--;
}

// Mini-activity
// Instruction: With given array, kindly display each elements on the console using do-while loop

let indexNumberA = 0;

let computerBrands = ['Apple Macbook Pro','HP Notebook','Asus','Lenovo','Acer','Dell','Huawei'];
do{
	console.log(computerBrands[indexNumberA]);
	indexNumberA++;
}while(indexNumberA <= computerBrands.length-1);

// For Loop
 // variable - the scope of the declared variable is within the for loop
for(let count=5;count >=0; count--){
	console.log(count);
}

// Instruction: Given an Array, kindly print each element using for loop

let colors = ['Red','Green','Blue','Yellow','Purple','White','Black'];

for(let i = 0; i <= colors.length - 1;i++){
	console.log(colors[i]);
}

// Continue and Break
// Break - stops the executuin of our code
// Continue - skip a block code and continue to the next iteration

/*

  18, 19, 20, 21, 24, 25

  age == 21(debutant age boys), we will skip then go to the next iteration

  18, 19, 20, 24, 25 */

 let ages = [18, 19, 20, 21, 24, 25];
 // Skip the debutant age of boys and girs using continue keyword
              //0 <=5? true
 for(let i = 0; i <= ages.length - 1; i++){
 	          //ages[0] ->18
 	if(ages[i] == 21 || ages[i] == 18){
 		continue;
 	}
 	console.log(ages[i]);
 }

/* let studentNames = ['Den','Jayson','Marvin','Rommel'];

once we found Jayson on our array, we will stop the loop

Den
Jayson

*/
 let studentNames = ['Den','Jayson','Marvin','Rommel'];
 // 0 <= 3? true
for (let i = 0; i <= studentNames.length -1; i++) {
	//studentNames[0] = > Den
	if(studentNames[i] == 'Jayson'){
		break;
	
	
	}
	console.log(studentNames[i])
}

//Coding Challenge - Scope (Loops, Continue and Break)
// You can add the solution under our s16/d1/index.js only, no need to create a separate folder for this. Kindly push your solutions once you are done
//Then link your activity gitlab repo under Boodle WD078-16

/*
Instructions:

1. Given the array 'adultAge', display only adult age range on the console.

-- the goal of this activity is to exclude values that are not in the range of adult age. Using the tool loops, continue or break, 
the students must display only the given sample output below on their console.
*/
let adultAge = [20, 23, 33, 27, 18, 19, 70, 15, 55, 63, 85, 12, 19];

/*
	Sample output:
  
  20
  23
  33
  27
  70
  55
  63
  85
*/




for(let x = 0; x <= adultAge.length-1 ; x++){
	if (adultAge[x] == 18 || adultAge[x] == 19 || adultAge[x] == 15 || adultAge[x] == 12 || adultAge[x] == 19){

		continue;
	}
	console.log(adultAge[x]);
}

/*
Instructions:

2. Given an array 'students' and a function searchStudent, create a solution that, once a function is invoked with a given name of student as its argument,
	the function will start to loop and search for the student on a given array and once there's found it will print it on the console 
  and stop the execution of loop. 
  
  -- the goal of this activity is to print only the value needed based on the argument given. Use loop, and continue or break
*/
let students = ['Gary', 'Amelie', 'Anne', 'Jazz', 'Preina', 'James', 'Kelly', 'Diane', 'Lucy', 'Vanessa', 'Kim', 'Francine'];

function searchStudent(studentName){
	for(let y = 0; y <= students.length-1; y++){
	if(students[y] == "Jazz"){
	console.log(students[y]);
	break;
     }
   }
}
searchStudent('Jazz'); //invoked function with a given argument 'Jazz'

/*
	Sample output:
  
  Jazz
*/





